exports.post = (req, res, next) => {
    console.log('req.params');
    console.log(req.params);
    // console.log('res');
    // console.log(res);
    // console.log('next');
    // console.log(next);
    res.status(201).send('Requisição de insert recebida com sucesso!');
};
exports.put = (req, res, next) => {
    var id = req.params.id;
    res.status(201).send(`Requisição de edit recebida com sucesso! ${id}`);
};
exports.delete = (req, res, next) => {
    var id = req.params.id;
    res.status(200).sendStatus(`Requisição recebida com sucesso! ${id}`);
};
exports.options = (req, res, next) => {
    // var id = req.params.id;
    res.status(200).sendStatus(`Requisição recebida com sucesso!`);
};
